﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    class Program
    {
        public static string Rubkop(double n)
        {

            double n1 = Math.Round(n - Math.Truncate(n), 2) * 100;
            if (n1 % 100 == 0) { n1 = 0.0; n = n + 1; }
            string str = Convert.ToString(Math.Truncate(n) + " руб " + n1 + " коп");
            return str;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Введите номер задачи 1-4");
            int caseSwitch = Convert.ToInt32(Console.ReadLine());

            switch (caseSwitch)
            {
                case 1:
                    double n = 23.16809;
                    n = Math.Round(n - Math.Truncate(n), 3) * 1000;
                    string str = Convert.ToString(n);
                    n = 0;
                    for (int i = 0; i < str.Length; i++)
                    {
                        n += double.Parse(str[i].ToString());

                    }
                    Console.WriteLine(n);
                    break;
                case 2:
                    Console.WriteLine("Вычисление стоимости поездки на дачу.");
                    Console.WriteLine("Расстояние до дачи (км) ");
                    double s = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine("количество бензина, которое потребляет автомобиль на 100 км пробега;");
                    double benz = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine("цена одного литра бензина");
                    double cena = Convert.ToDouble(Console.ReadLine());
                    double rub = ((s / 100 * benz * cena) * 2);

                    Console.WriteLine(Rubkop(rub));
                    break;

                case 3:
                    Console.WriteLine("Введите а");
                    int a = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Введите b");
                    int b = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Введите c");
                    int c = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Введите m");
                    int m = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Введите N");
                    int N = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Введите d");
                    int d = Convert.ToInt32(Console.ReadLine());


                    if (a * d * d + b * d + c == 0 && m * d + N == 0)
                    {
                        Console.WriteLine("True");
                    }
                    else
                        Console.WriteLine("False");
                    break;
                case 4:
                    double chislo = Double.Parse(Console.ReadLine().Trim('#'));
                    Console.WriteLine(Rubkop(chislo));
                    break;
                default:
                    Console.WriteLine("Задача была выбрана неправильно");
                    break;
            }
            Console.ReadKey();
        }

    }
}
